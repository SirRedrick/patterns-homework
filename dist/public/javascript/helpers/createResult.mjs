import { createElement } from './createElement.mjs';

const createResult = ({ name }, place) => {
  return createElement({
    tagName: 'div',
    attributes: {
      id: `place-${place}`
    },
    children: [place.toString(), name]
  });
};

export { createResult };
