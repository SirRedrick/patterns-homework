import { Hub, Room } from './components/index.mjs';

const username = sessionStorage.getItem('username');
if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

const hub = new Hub(socket);
const room = new Room(socket, username);

const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');

// Functions
const switchViewToRoom = () => {
  roomsPage.classList.add('display-none');
  gamePage.classList.remove('display-none');
};

const switchViewToHub = () => {
  roomsPage.classList.remove('display-none');
  gamePage.classList.add('display-none');
};

// Socket Events

// Login Events
socket.on('LOGIN_SUCCESS', rooms => hub.updateRooms(rooms));

socket.on('LOGIN_FAILURE', reason => {
  alert(reason);
  sessionStorage.removeItem('username');
  window.location.replace('/login');
});

// Hub Events
socket.on('HUB_UPDATE_ROOMS', rooms => hub.updateRooms(rooms));

socket.on('HUB_CREATE_ROOM_SUCCESS', newRoomName => {
  socket.emit('HUB_JOIN_ROOM', newRoomName);
});

socket.on('HUB_CREATE_ROOM_FAILURE', reason => alert(reason));

socket.on('HUB_JOIN_ROOM_SUCCESS', currentRoom => {
  switchViewToRoom();
  room.join(currentRoom);
});

socket.on('HUB_JOIN_ROOM_FAILURE', reason => alert(reason));

socket.on('HUB_LEAVE_ROOM_SUCCESS', () => switchViewToHub());

// Room events
socket.on('ROOM_UPDATE', ({ users }) => room.updateUsers(users));

socket.on('ROOM_EVERYONE_READY', textId => room.startCountdown(textId));

socket.on('ROOM_TICK', timeLeft => room.tick(timeLeft));

socket.on('ROOM_GAME_START', () => room.startGame());

socket.on('ROOM_GAME_END', leaderBoard => room.endGame(leaderBoard));

socket.on('ROOM_ERR', message => {
  alert(message);
  switchViewToHub();
});

// Commentator Events
socket.on('COMMENTATOR', message => room.comment(message));
