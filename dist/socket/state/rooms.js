"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rooms = void 0;
const room_1 = require("./room");
class Rooms {
    constructor() {
        this.rooms = [];
    }
    getRooms() {
        return this.rooms;
    }
    updateRooms(newRooms) {
        this.rooms = newRooms;
    }
    createRoom(name) {
        if (!this.isNameDuplicate(name)) {
            this.rooms.push(new room_1.Room(name));
        }
        else {
            throw Error(`Room with ${name} name already exists`);
        }
    }
    getRoom(name) {
        const room = this.rooms.find(r => r.name === name);
        if (room) {
            return room;
        }
        else {
            throw Error(`Room with name ${name} does not exist`);
        }
    }
    updateRoom(newRoom) {
        this.rooms = this.rooms.map(room => (room.name === newRoom.name ? newRoom : room));
    }
    joinRoom(name, username) {
        this.getRoom(name).join(username);
    }
    resetRoom(name) {
        this.getRoom(name).reset();
    }
    removeRoom(name) {
        this.rooms = this.rooms.filter(room => room.name !== name);
    }
    getLeaderBoard(name) {
        return this.getRoom(name).leaderBoard;
    }
    updateProgress(name, username, progress) {
        this.getRoom(name).updateProgress(username, progress);
    }
    addDnfToLeaderBoard(name) {
        this.getRoom(name).addDnfToLeaderBoard();
    }
    removeUser(roomName, username) {
        this.getRoom(roomName).removeUser(username);
    }
    isNameDuplicate(name) {
        const result = this.rooms.find(room => room.name === name);
        return Boolean(result);
    }
    toggleReady(name, username) {
        this.getRoom(name).toggleReady(username);
    }
    isRoomReady(name) {
        return this.getRoom(name).isEveryoneReady();
    }
    isRoomFinished(name) {
        return this.getRoom(name).isFinished();
    }
    toggleRoomRunning(name) {
        this.getRoom(name).toggleRunning();
    }
}
exports.Rooms = Rooms;
