"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Timer = void 0;
class Timer {
    constructor(duration, subscribers = [], subscribedOnEnd = []) {
        this.duration = duration;
        this.subscribers = subscribers;
        this.subscribedOnEnd = subscribedOnEnd;
        this.interval = null;
        this.startTime = 0;
    }
    start() {
        this.startTime = Date.now();
        this.tick(this.duration);
        this.interval = setInterval(() => {
            const delta = Date.now() - this.startTime;
            const timeLeft = Math.max(this.duration - delta, 0);
            this.tick(timeLeft);
            if (timeLeft === 0) {
                this.stop();
            }
        }, 1000);
    }
    stop() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval.unref();
        }
        this.subscribedOnEnd.forEach(sub => sub());
    }
    tick(timeLeft) {
        this.subscribers.forEach(sub => sub(timeLeft));
    }
    subscribe(fn) {
        this.subscribers.push(fn);
    }
    unsubscribe(fn) {
        this.subscribers = this.subscribers.filter(sub => sub !== fn);
    }
    subscribeOnEnd(fn) {
        this.subscribedOnEnd.push(fn);
    }
    unsubscribeFromEnd(fn) {
        this.subscribedOnEnd = this.subscribedOnEnd.filter(sub => sub !== fn);
    }
}
exports.Timer = Timer;
