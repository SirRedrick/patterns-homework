"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCurrentRoom = void 0;
const getCurrentRoom = (socket) => {
    const roomsIter = socket.rooms.values();
    roomsIter.next();
    return roomsIter.next().value;
};
exports.getCurrentRoom = getCurrentRoom;
