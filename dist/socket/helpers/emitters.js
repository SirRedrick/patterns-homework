"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.socketEmitters = exports.ioEmitters = void 0;
const fp_1 = __importDefault(require("lodash/fp"));
const ioEmitters = (io) => {
    return {
        toAll: fp_1.default.curry((eventType, data) => io.emit(eventType, data)),
        toRoom: fp_1.default.curry((name, eventType, data) => {
            if (data === undefined) {
                io.to(name).emit(eventType);
            }
            else {
                io.to(name).emit(eventType, data);
            }
        })
    };
};
exports.ioEmitters = ioEmitters;
const socketEmitters = (socket) => {
    return {
        toSocket: fp_1.default.curry((eventType, data) => socket.emit(eventType, data)),
        toRoom: fp_1.default.curry((name, eventType, data) => {
            if (data === undefined) {
                socket.to(name).emit(eventType);
            }
            else {
                socket.to(name).emit(eventType, data);
            }
        })
    };
};
exports.socketEmitters = socketEmitters;
