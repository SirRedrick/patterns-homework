"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.room = void 0;
const helpers_1 = require("../helpers");
const room = (io, socket, username, rooms, runningTimers, commentators, startCountdown) => {
    const ioEmit = helpers_1.ioEmitters(io);
    const socketEmit = helpers_1.socketEmitters(socket);
    socket.on('ROOM_TOGGLE_READY', () => {
        const name = helpers_1.getCurrentRoom(socket);
        rooms.toggleReady(name, username);
        if (rooms.isRoomReady(name)) {
            startCountdown(name);
        }
        socketEmit.toSocket('ROOM_TOGGLE_READY_SUCCESS');
        ioEmit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
    });
    socket.on('ROOM_UPDATE_PROGRESS', progress => {
        var _a, _b;
        const name = helpers_1.getCurrentRoom(socket);
        try {
            rooms.updateProgress(name, username, progress);
        }
        catch (err) {
            socketEmit.toSocket('ROOM_ERROR')(err.message);
        }
        if (progress === 100) {
            (_a = commentators.get(name)) === null || _a === void 0 ? void 0 : _a.finishes(username);
        }
        if (rooms.isRoomFinished(name)) {
            const timer = runningTimers.get(name);
            if (timer) {
                timer.stop();
                runningTimers.delete(name);
            }
            ioEmit.toRoom(name)('ROOM_GAME_END')(rooms.getLeaderBoard(name));
            (_b = commentators.get(name)) === null || _b === void 0 ? void 0 : _b.raceEnds(username);
            rooms.resetRoom(name);
            ioEmit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
        }
        ioEmit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
    });
    socket.on('ROOM_THIRTY_SYMBOLS', () => {
        var _a;
        const name = helpers_1.getCurrentRoom(socket);
        (_a = commentators.get(name)) === null || _a === void 0 ? void 0 : _a.onFinalStraight();
    });
};
exports.room = room;
