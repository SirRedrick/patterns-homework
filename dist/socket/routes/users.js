"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.users = void 0;
const helpers_1 = require("../helpers");
const users = (socket, username, activeUsers, rooms) => {
    const socketEmit = helpers_1.socketEmitters(socket);
    if (activeUsers.has(username)) {
        return socketEmit.toSocket('LOGIN_FAILURE')('Specified username has been already taken');
    }
    activeUsers.add(username);
    socketEmit.toSocket('LOGIN_SUCCESS')(rooms.getRooms());
    socket.on('disconnect', () => {
        activeUsers.delete(username);
    });
};
exports.users = users;
