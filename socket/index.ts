import { Server } from 'socket.io';
import * as config from './config';
import { Rooms } from './state/rooms';
import { Timer } from './state/timer';
import { hub, users, room, Commentator } from './routes';
import { getRandomTextId, ioEmitters } from './helpers';

export default (io: Server) => {
  const activeUsers = new Set<string>();
  const runningTimers = new Map<string, Timer>();
  const commentators = new Map<string, Commentator>();
  const rooms = new Rooms();

  const emit = ioEmitters(io);

  const tick = (name: string, commentator: Commentator) => (time: number) => {
    const timeLeft = Math.round(time / 1000);
    emit.toRoom(name)('ROOM_TICK')(timeLeft);
    commentator.onTick(timeLeft);
  };

  const endGame = (name: string, commentator: Commentator) => () => {
    rooms.addDnfToLeaderBoard(name);

    emit.toRoom(name)('ROOM_GAME_END')(rooms.getLeaderBoard(name));
    commentator.raceEnds();

    rooms.resetRoom(name);

    emit.toRoom(name)('ROOM_UPDATE')(rooms.getRoom(name));
    emit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
  };

  const startGame = (name: string, commentator: Commentator) => () => {
    emit.toRoom(name)('ROOM_GAME_START')();
    commentator.switchMode();

    const onTick = [tick(name, commentator)];
    const onEnd = [endGame(name, commentator)];

    const timer = new Timer(config.SECONDS_FOR_GAME * 1000, onTick, onEnd);
    timer.start();
    runningTimers.set(name, timer);
  };

  const startCountdown = (name: string) => {
    emit.toAll('HUB_UPDATE_ROOMS')(rooms.getRooms());
    emit.toRoom(name)('ROOM_EVERYONE_READY')(getRandomTextId());

    const commentator = new Commentator(io, name, rooms);
    const onTick = [tick(name, commentator)];
    const onEnd = [startGame(name, commentator)];

    const timer = new Timer(config.SECONDS_TIMER_BEFORE_START_GAME * 1000, onTick, onEnd);
    timer.start();
    runningTimers.set(name, timer);
  };

  io.on('connection', socket => {
    const username = socket.handshake.query.username as string;

    users(socket, username, activeUsers, rooms);
    hub(io, socket, username, rooms, runningTimers, startCountdown);
    room(io, socket, username, rooms, runningTimers, commentators, startCountdown);
  });
};
