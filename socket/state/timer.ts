class Timer {
  private readonly duration: number;

  private interval: NodeJS.Timeout | null;
  private subscribers: Array<(timeLeft: number) => void>;
  private subscribedOnEnd: Array<() => void>;
  private startTime: number;

  constructor(
    duration: number,
    subscribers: Array<(timeLeft: number) => void> = [],
    subscribedOnEnd: Array<() => void> = []
  ) {
    this.duration = duration;
    this.subscribers = subscribers;
    this.subscribedOnEnd = subscribedOnEnd;

    this.interval = null;
    this.startTime = 0;
  }

  start() {
    this.startTime = Date.now();
    this.tick(this.duration);

    this.interval = setInterval(() => {
      const delta = Date.now() - this.startTime;
      const timeLeft = Math.max(this.duration - delta, 0);
      this.tick(timeLeft);

      if (timeLeft === 0) {
        this.stop();
      }
    }, 1000);
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval.unref();
    }

    this.subscribedOnEnd.forEach(sub => sub());
  }

  private tick(timeLeft: number) {
    this.subscribers.forEach(sub => sub(timeLeft));
  }

  subscribe(fn: (timeLeft: number) => void): void {
    this.subscribers.push(fn);
  }

  unsubscribe(fn: (timeLeft: number) => void): void {
    this.subscribers = this.subscribers.filter(sub => sub !== fn);
  }

  subscribeOnEnd(fn: () => void) {
    this.subscribedOnEnd.push(fn);
  }

  unsubscribeFromEnd(fn: () => void) {
    this.subscribedOnEnd = this.subscribedOnEnd.filter(sub => sub !== fn);
  }
}

export { Timer };
