import { Room } from './room';
import { User } from '../../types';

class Rooms {
  private rooms: Array<Room>;

  constructor() {
    this.rooms = [];
  }

  getRooms(): Room[] {
    return this.rooms;
  }

  updateRooms(newRooms: Room[]): void {
    this.rooms = newRooms;
  }

  createRoom(name: string) {
    if (!this.isNameDuplicate(name)) {
      this.rooms.push(new Room(name));
    } else {
      throw Error(`Room with ${name} name already exists`);
    }
  }

  getRoom(name: string): Room {
    const room = this.rooms.find(r => r.name === name);

    if (room) {
      return room;
    } else {
      throw Error(`Room with name ${name} does not exist`);
    }
  }

  updateRoom(newRoom: Room): void {
    this.rooms = this.rooms.map(room => (room.name === newRoom.name ? newRoom : room));
  }

  joinRoom(name: string, username: string) {
    this.getRoom(name).join(username);
  }

  resetRoom(name: string): void {
    this.getRoom(name).reset();
  }

  removeRoom(name: string): void {
    this.rooms = this.rooms.filter(room => room.name !== name);
  }

  getLeaderBoard(name: string): User[] {
    return this.getRoom(name).leaderBoard;
  }

  updateProgress(name: string, username: string, progress: number): void {
    this.getRoom(name).updateProgress(username, progress);
  }

  addDnfToLeaderBoard(name: string): void {
    this.getRoom(name).addDnfToLeaderBoard();
  }

  removeUser(roomName: string, username: string): void {
    this.getRoom(roomName).removeUser(username);
  }

  isNameDuplicate(name: string): boolean {
    const result = this.rooms.find(room => room.name === name);
    return Boolean(result);
  }

  toggleReady(name: string, username: string): void {
    this.getRoom(name).toggleReady(username);
  }

  isRoomReady(name: string): boolean {
    return this.getRoom(name).isEveryoneReady();
  }

  isRoomFinished(name: string): boolean {
    return this.getRoom(name).isFinished();
  }

  toggleRoomRunning(name: string): void {
    this.getRoom(name).toggleRunning();
  }
}

export { Rooms };
