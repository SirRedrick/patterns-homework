import { User } from '../../types/user';
import * as config from '../config';

class Room {
  public name: string;
  users: User[];
  leaderBoard: User[];
  isRunning: boolean;

  constructor(name: string) {
    this.name = name;

    this.users = [];
    this.leaderBoard = [];
    this.isRunning = false;
  }

  update({ name, users, leaderBoard, isRunning }: Room) {
    this.name = name;
    this.users = users;
    this.leaderBoard = leaderBoard;
    this.isRunning = isRunning;
  }

  reset() {
    this.users = this.users.map(user => ({ ...user, isReady: false, progress: 0 }));
    this.isRunning = false;
    this.leaderBoard = [];
  }

  toggleRunning() {
    this.isRunning = !this.isRunning;
  }

  join(username: string) {
    if (this.users.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      throw new Error(`Room is full, maximum amount is ${config.MAXIMUM_USERS_FOR_ONE_ROOM}`);
    }

    this.users.push({
      name: username,
      isReady: false,
      progress: 0
    });
  }

  removeUser(name: string) {
    this.users = this.users.filter(user => user.name !== name);
    this.leaderBoard = this.leaderBoard.filter(user => user.name !== name);
  }

  toggleReady(username: string) {
    this.users = this.users.map(user =>
      user.name === username ? { ...user, isReady: !user.isReady } : user
    );

    if (this.isEveryoneReady()) {
      this.isRunning = true;
    }
  }

  isEveryoneReady(): boolean {
    return this.users.every(user => user.isReady);
  }

  isFinished(): boolean {
    return this.users.every(user => user.progress === 100);
  }

  updateProgress(username: string, progress: number) {
    this.users = this.users.map(user => (user.name === username ? { ...user, progress } : user));

    if (progress === 100) {
      this.leaderBoard.push(this.users.find(u => u.name === username)!);
    }
  }

  addDnfToLeaderBoard() {
    this.leaderBoard = this.leaderBoard.concat(this.getDNF());
  }

  getDNF() {
    return this.users
      .filter(user => user.progress < 100)
      .sort((user_a, user_b) => user_b.progress - user_a.progress);
  }
}

export { Room };
