import { Server } from 'socket.io';
import { Rooms } from '../state/rooms';
import { User } from '../../types';
import { commentator } from '../../data';
import { ioEmitters } from '../helpers';

class Commentator {
  io: Server;
  ioEmit;
  roomName: string;
  rooms: Rooms;
  racers: { [name: string]: string };
  racerTime: { [name: string]: number };

  mode: 'countdown' | 'race';
  time: number;

  constructor(io: Server, roomName: string, rooms: Rooms) {
    this.io = io;
    this.ioEmit = ioEmitters(io);
    this.roomName = roomName;
    this.rooms = rooms;
    this.racers = rooms
      .getRoom(this.roomName)
      .users.reduce((acc: { [name: string]: string }, user: User) => {
        acc[user.name] = commentator.cars[Math.floor(Math.random() * commentator.cars.length)];
        return acc;
      }, {});

    this.mode = 'countdown';
    this.time = 0;
    this.racerTime = {};
  }

  onTick(timeLeft: number) {
    switch (this.mode) {
      case 'countdown':
        this.onCountdown(timeLeft);
        break;
      case 'race':
        this.onRace(timeLeft);
        break;
    }
    this.time = 60 - timeLeft;
  }

  onCountdown(timeLeft: number) {
    switch (timeLeft) {
      case 10:
        this.intro();
        break;
      case 5:
        this.announceRacers();
        break;
      case 0:
        this.ioEmit.toRoom(this.roomName)('COMMENTATOR')('Погнали!');
        break;
    }
  }

  intro() {
    this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(commentator.intro);
  }

  announceRacers() {
    const racerPhrases = Object.entries(this.racers)
      .map(([name, car], index) => `${name} на ${car} под номером ${index + 1}`)
      .join(', ');

    this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(commentator.announcements + racerPhrases);
  }

  onRace(timeLeft: number) {
    switch (timeLeft) {
      case 30:
        this.status();
    }
  }

  status() {
    const racers = this.rooms
      .getRoom(this.roomName)
      .users.sort((user_a: User, user_b: User) => user_b.progress - user_a.progress)
      .slice(0, 3);

    const phrases = racers
      ?.map((racer: User, index: number) => {
        switch (index) {
          case 0:
            return `${racer.name} на ${this.racers[racer.name]} сейчас первый`;
          case 1:
            return `за ним идет ${racer.name}`;
          case 2:
            return `а третьим идет ${racer.name}...`;
        }
      })
      .join(', ');

    this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(phrases);
  }

  onFinalStraight() {
    const racers = this.rooms
      .getRoom(this.roomName)
      ?.users.sort((user_a: User, user_b: User) => user_b.progress - user_a.progress)
      .slice(0, 3);

    const phrases =
      racers
        .map((racer: User, index: number) => {
          switch (index) {
            case 0:
              return `До финиша осталось совсем немного и похоже что первым его может пересечь ${
                racer.name
              } на своем ${this.racers[racer.name]}.`;
            case 1:
              return ` Второе место может достаться ${racer.name}`;
            case 2:
              return `или ${racer.name}`;
          }
        })
        .join(' ') + '. Но давайте дождемся финиша.';

    this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(phrases);
  }

  finishes(name: string) {
    this.racerTime[name] = this.time;
    this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(`Финишную прямую пересекает ${name}`);
  }

  raceEnds(name?: string) {
    const racers = this.rooms
      .getRoom(this.roomName)
      ?.leaderBoard.filter((racer: User) => racer.progress === 100)!;

    const phrases = racers
      ?.map((racer: User, index: number) => {
        switch (index) {
          case 0:
            return `первое место занимает ${racer.name} за рекордные ${
              this.racerTime[racer.name]
            } секунд(ы)`;
          case 1:
            return `второе место - ${racer.name} за ${this.racerTime[racer.name]} секунд(ы)`;
          case 2:
            return `третье -  ${racer.name} со всемнем ${this.racerTime[racer.name]} секунд(ы)`;
        }
      })
      .join(', ');

    this.ioEmit.toRoom(this.roomName)('COMMENTATOR')(
      name
        ? `Финишную прямую пересекает ${name} и финальный результат: ${phrases}`
        : `Время вышло и финальный результат: ${phrases}`
    );
  }

  switchMode() {
    this.mode = this.mode === 'countdown' ? 'race' : 'countdown';
  }
}

export { Commentator };
