import { texts } from '../../data';

const getRandomTextId = (): number => Math.floor(Math.random() * texts.length);

export { getRandomTextId };
