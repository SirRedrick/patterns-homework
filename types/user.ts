type User = {
  name: string;
  isReady: boolean;
  progress: number;
};

export { User };
