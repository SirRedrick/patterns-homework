import { createElement, createUser, createResult } from '../helpers/index.mjs';

class Room {
  constructor(socket, username) {
    this.socket = socket;
    this.user = username;

    this.titleElement = document.getElementById('room-name');
    this.usersContainer = document.getElementById('users');
    this.timer = document.getElementById('timer');
    this.textContainer = document.getElementById('text-container');
    this.modal = document.getElementById('modal');
    this.resultsContainer = document.getElementById('results-container');
    this.comments = document.getElementById('comments');
    this.buttons = {
      ready: document.getElementById('ready-btn'),
      quit: document.getElementById('quit-room-btn'),
      close: document.getElementById('quit-results-btn')
    };

    this.leave = this.leave.bind(this);
    this.ready = this.ready.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.closeResults = this.closeResults.bind(this);

    this.buttons.ready.addEventListener('click', this.ready);
    this.buttons.quit.addEventListener('click', this.leave);
    this.buttons.close.addEventListener('click', this.closeResults);

    this.name = null;
    this.text = null;
    this.chars = null;
    this.currentChar = 0;
  }

  join(room) {
    this.name = room.name;
    this.titleElement.innerText = room.name;

    this.updateUsers(room.users);
  }

  updateUsers(users) {
    const user = users.find(user => user.name === this.user);
    const userElements = users.map(user => createUser(user, this.user));

    this.usersContainer.innerHTML = '';
    this.usersContainer.append(...userElements);

    this.buttons.ready.innerText = user.isReady ? 'NOT READY' : 'READY';
  }

  async startCountdown(textId) {
    this.text = await fetch(`/game/texts/${textId}`).then(res => res.text());

    this.buttons.quit.classList.add('display-none');
    this.buttons.ready.classList.add('display-none');
  }

  startGame() {
    this.updateText(this.text);
    window.addEventListener('keydown', this.onKeyPress);
  }

  endGame(leaderBoard) {
    const resultElements = leaderBoard.map((user, index) => createResult(user, index));

    this.resultsContainer.append(...resultElements);
    this.modal.classList.remove('display-none');
  }

  updateText(text) {
    const textElements = text.split('').map(char =>
      createElement({
        tagName: 'span',
        children: [char]
      })
    );

    this.chars = textElements;
    this.textContainer.append(...textElements);
    this.textContainer.classList.remove('display-none');
  }

  onKeyPress(e) {
    const char = this.chars[this.currentChar];
    const nextChar = this.chars[this.currentChar + 1];

    if (e.key === char.textContent) {
      char.classList.remove('current-char');
      char.classList.add('correct-char');

      nextChar?.classList.add('current-char');
      this.currentChar++;

      this.socket.emit(
        'ROOM_UPDATE_PROGRESS',
        Math.round((this.currentChar / this.chars.length) * 100)
      );

      if (this.chars.length - this.currentChar === 30) {
        this.socket.emit('ROOM_THIRTY_SYMBOLS');
      }

      if (Math.round((this.currentChar / this.chars.length) * 100 === 100)) {
        window.removeEventListener('keydown', this.onKeyPress);
      }
    }
  }

  leave() {
    this.socket.emit('HUB_LEAVE_ROOM');
  }

  ready() {
    this.socket.emit('ROOM_TOGGLE_READY');
  }

  tick(time) {
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;

    const text =
      minutes > 0 ? `${minutes.toString()}:${seconds.toString().padStart(2, '0')}` : seconds;
    this.timer.textContent = text;
  }

  closeResults() {
    this.resultsContainer.innerHTML = '';
    this.modal.classList.add('display-none');
    this.timer.textContent = '';
    this.textContainer.innerHTML = '';
    this.buttons.ready.classList.remove('display-none');
    this.buttons.quit.classList.remove('display-none');
    this.chars = null;
    this.currentChar = 0;
    window.removeEventListener('keydown', this.onKeyPress);
  }

  comment(comment) {
    this.comments.textContent = comment;
  }
}

export { Room };
